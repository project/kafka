<?php

/**
 * @file
 * Example settings.local fragment, to copy or include to use defaults.
 */

// That closure contains your complete Kafka module configuration.
$configure_kafka = function (array $settings) {
  $settings['queue_default'] = 'queue.kafka';

  // Most Kafka instances will neither have this topic nor allow auto-creation,
  // so leave it in database. It does not need Kafka bandwidth anyway.
  $settings['queue_service_update_fetch_tasks'] = 'queue.database';

  // Add lines like this one for each topic available in Kafka.
  /* $settings['queue_service_{queue_name}'] = 'queue.kafka'; */

  $settings['kafka'] = [
    'consumer' => [
      'brokers' => ['127.0.0.1'],
    ],
    'producer' => [
      'brokers' => ['127.0.0.1'],
    ],
  ];

  return $settings;
};

// Enable/disable Kafka by enabling or commenting out the following line.
$settings = $configure_kafka($settings);

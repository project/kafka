<?php

namespace Drupal\kafka\Commands;

use Drupal\Core\Queue\QueueFactory;
use Drupal\kafka\ClientFactory;
use Drush\Commands\DrushCommands;
use RdKafka\Conf;
use RdKafka\Consumer;
use RdKafka\KafkaConsumer;
use RdKafka\Producer;
use RdKafka\TopicConf;
use RdKafka\TopicPartition;
use Symfony\Component\Yaml\Yaml;

/**
 * Kafka Drush commands.
 */
class KafkaCommands extends DrushCommands {

  const DEMO_TOPIC = 'aggregator_feeds';

  /**
   * Client factory.
   *
   * @var \Drupal\kafka\ClientFactory
   */
  protected $clientFactory;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Kafka producer.
   *
   * @var \RdKafka\Producer
   */
  protected $producer;

  /**
   * High-level Kafka consumer.
   *
   * @var \RdKafka\KafkaConsumer
   */
  protected $highLevelConsumer;

  /**
   * Low-level Kafka consumer.
   *
   * @var \RdKafka\Consumer
   */
  protected $lowLevelConsumer;

  /**
   * CommandProvider constructor.
   *
   * @param \Drupal\kafka\ClientFactory $clientFactory
   *   Client factory.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   Queue factory.
   * @param \RdKafka\Producer $producer
   *   Kafka producer.
   * @param \RdKafka\KafkaConsumer $highLevelConsumer
   *   High level Kafka consumer.
   * @param \RdKafka\Consumer $lowLevelConsumer
   *   Low level Kafka consumer.
   */
  public function __construct(
    ClientFactory $clientFactory,
    QueueFactory $queueFactory,
    Producer $producer,
    KafkaConsumer $highLevelConsumer,
    Consumer $lowLevelConsumer
  ) {
    $this->clientFactory = $clientFactory;
    $this->queueFactory = $queueFactory;
    $this->producer = $producer;
    $this->highLevelConsumer = $highLevelConsumer;
    $this->lowLevelConsumer = $lowLevelConsumer;
  }

  /**
   * Helper for high-level consumer: dump the partition list.
   *
   * @param arrayint\RdKafka\TopicPartition $partitions
   *   An array of the partitions by id.
   * @param null|string $label
   *   Optional. A label for the display.
   */
  public function dumpPartitions(array $partitions, $label = NULL) {
    if ($label) {
      /** @var \RdKafka\TopicPartition $partition */
      $partition = reset($partitions);
      $this->writeln($label . $partition->getTopic() . "\n");
    }
    $this->writeln(implode('', array_map(function (TopicPartition $partition) {
        return sprintf('%4s:%-8s%s', $partition->getPartition(),
          $partition->getOffset(),
          ($partition->getPartition() + 1) % 10 ? '' : "\n");
    }, $partitions)) . "\n");
  }

  /**
   * Demoes the high-level Kafka consumer.
   *
   * @command kafka:highLevelConsumerDemo
   * @aliases khlcd
   */
  public function highLevelConsumerDemo() {
    $conf = new Conf();
    $t0 = time();
    // Set a rebalance callback to log partition assignments (optional)
    0 && $conf->setRebalanceCb(function (
      KafkaConsumer $kafka,
      $err,
      array $partitions = NULL
    ) {
      switch ($err) {
        case RD_KAFKA_RESP_ERR__ASSIGN_PARTITIONS:
          $this->dumpPartitions($partitions, "Assign on: ");
          $kafka->assign($partitions);
          break;

        case RD_KAFKA_RESP_ERR__REVOKE_PARTITIONS:
          $this->dumpPartitions($partitions, "Revoke on: ");
          $kafka->assign(NULL);
          break;

        default:
          throw new \Exception($err);
      }
    });

    // Set where to start consuming messages when there is no initial offset in
    // offset store or the desired offset is out of range.
    // 'smallest': start from the beginning.
    $conf->set('auto.offset.reset', 'latest');

    $consumer = $this->clientFactory->create('high', $conf);

    // Subscribe to topic 'drupal'.
    $consumer->subscribe([self::DEMO_TOPIC]);

    $this->io()
      ->writeln("Waiting for partition assignment... (make take some time when");
    $this->io()->writeln("quickly re-joining the group after leaving it.)");
    sleep(5);

    $this->io()->writeln(($t1 = time()));
    $count = 0;
    $continue = TRUE;
    while ($continue) {
      $message = $consumer->consume(100);
      switch ($message->err) {
        case RD_KAFKA_RESP_ERR_NO_ERROR:
          printf("TS: %d Part#: %d Len %d Offset %d Payload: %s\n",
            $message->timestamp, $message->partition, $message->len,
            $message->offset, $message->payload);
          $count++;
          break;

        case RD_KAFKA_RESP_ERR__PARTITION_EOF:
          $this->io()->writeln("No more messages; will not wait for more");
          $continue = FALSE;
          break;

        case RD_KAFKA_RESP_ERR__TIMED_OUT:
          $this->io()->writeln("Timed out");
          $continue = FALSE;
          break;

        default:
          throw new \Exception($message->errstr(), $message->err);
      }
    }

    $this->io()->writeln("count: $count");
    $this->io()->writeln(($t2 = time()));
    $this->io()
      ->writeln("setup: " . ($t1 - $t0) . " sec, delay: " . ($t2 - $t1));

    if ($count) {
      $consumer->commit();
    }
    $consumer->unsubscribe();
  }

  /**
   * Demoes the low-level Kafka consumer.
   *
   * @command kafka:lowLevelConsumerDemo
   * @aliases kllcd
   */
  public function lowLevelConsumerDemo() {
    $this->lowLevelConsumer->setLogLevel(LOG_WARNING);
    $debug = TRUE;

    $topicConf = new TopicConf();
    $topicConf->set('offset.store.method', 'broker');
    $topicConf->set('auto.offset.reset', 'latest');
    $topicConf->set('auto.commit.interval.ms', 100);
    /** @var \RdKafka\ConsumerTopic $topic */
    $topic = $this->lowLevelConsumer->newTopic(self::DEMO_TOPIC, $topicConf);

    // The first argument is the partition to consume from.
    // The second argument is the offset at which to start consumption. Valid
    // values are: RD_KAFKA_OFFSET_BEGINNING, RD_KAFKA_OFFSET_END,
    // RD_KAFKA_OFFSET_STORED.
    $topic->consumeStart(0, RD_KAFKA_OFFSET_END);

    $this->io()->writeln(($t0 = time()));
    $count = 0;
    while (TRUE) {
      $count++;
      // The first argument is the partition (again).
      // The second argument is the timeout.
      $msg = $topic->consume(0, 10 * 1000);
      if ($msg && $msg->err) {
        $this->io()->writeln($msg->errstr());
        break;
      }
      elseif ($debug) {
        $this->io()->writeln('Message: [' . $msg->payload . "]");
      }
    }
    $topic->consumeStop(0);
    $this->io()->writeln("count: $count");
    $this->io()->writeln(($t1 = time()));
    $this->io()->writeln("delay: " . ($t1 - $t0));
  }

  /**
   * Demoes the Kafka producer by submitting a number of messages.
   *
   * @command kafka:producerDemo
   * @aliases kpd
   */
  public function producerDemo() {
    $this->producer->setLogLevel(LOG_WARNING);
    /** @var \RdKafka\Topic $topic */
    $topic = $this->producer->newTopic('aggregator_feeds');

    $max = 1E5;
    $t0 = microtime(TRUE);
    for ($i = 1; $i <= $max; $i++) {
      if ($i % ($max / 10) === 0) {
        $this->io()->writeln("$i");
      }
      $topic->produce(RD_KAFKA_PARTITION_UA, 0,
        json_encode("Message $i (" . time() . ")"));
    }
    $t1 = microtime(TRUE);
    $rate = round($max / ($t1 - $t0));
    $this->io()->writeln("$max messages @ $rate msg/sec");
  }

  /**
   * Gets the available Kafka topics.
   *
   * @command kafka:topics
   * @aliases kt
   */
  public function topics() {
    $topics = $this->clientFactory->getTopics();
    $yamlTopics = Yaml::dump($topics);
    $this->io()->writeln($yamlTopics);
  }

  /**
   * Demos the Kafka producer through Queue API.
   *
   * @command kafka:queueProducerDemo
   * @aliases kqpd
   */
  public function queueProducerDemo() {
    $q = $this->queueFactory->get(self::DEMO_TOPIC);
    // Ensure queue exists and it not deleted.
    $q->createQueue();
    $t0 = microtime(TRUE);
    $max = 10;
    foreach (range(1, $max) as $i) {
      if ($i % ceil($max / 10) === 0) {
        $this->io()->writeln("$i");
      }
      $q->createItem("Message $i (" . time() . ")");
    }
    $t1 = microtime(TRUE);
    $rate = round($max / ($t1 - $t0));
    $this->io()->writeln("$max messages @ $rate msg/sec");
  }

  /**
   * Demos the Kafka high-level consumer through Queue API.
   *
   * @command kafka:queueConsumerDemo
   * @aliases kqcd
   */
  public function queueConsumerDemo() {
    $q = $this->queueFactory->get(self::DEMO_TOPIC);
    // Ensure queue exists and it not deleted.
    $q->createQueue();
    $t1 = $t0 = microtime(TRUE);
    $count = 0;
    $oldLap = $t0;
    while ($item = $q->claimItem(1)) {
      if ($count === 0) {
        // Check the time to first claim, caused by Kafka allocation.
        $t1 = microtime(TRUE);
      }
      $q->deleteItem($item);
      $newLap = microtime(TRUE);
      if ($newLap - $oldLap > 1) {
        $oldLap = $newLap;
        $this->io()->writeln("$count");
      }
      $count++;
    }
    $t2 = microtime(TRUE);
    $this->io()->writeln("count: $count");
    $this->io()
      ->writeln("setup: " . round($t1 - $t0) . " sec, delay: " . round($t2 - $t1));
    if ($count && ($t2 > $t1)) {
      $this->io()
        // Use ceil() to avoid rounding to 0.
        ->writeln(" = " . (($count - 1) / ceil($t2 - $t1)) . " items/sec");
    }
    $this->io()->writeln(PHP_EOL);
  }

}
